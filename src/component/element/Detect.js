import React from 'react'
import Component from 'ibeacon/src/component/Component'
import { View as Div } from 'react-native'

import Beacon from 'ibeacon/src/utility/Beacon'

export default class Detect extends Component {

  init(app){
    const beacon = new Beacon()
    beacon.set(app)
  }

  content({ text, store: { ui: { style, color }, read: { beacons, region } } }, { title, value, onChange, options, detail, uuid }){
    const detected = beacons.length > 0
    const target = !uuid? beacons[0]:
      beacons.map(beacon => { return (beacon.uuid === uuid? beacon: null) }).filter( item =>{ return item != null })[0]
    return(
      <Div style={{ ...this.size(0.35, detail?0.3:0.04), ...style.flexColSC, backgroundColor: color.grey[0] }}>
        {detected && detail && text.field({ text:
          'UUID: ' + target.uuid + '\n' +
          'Name: ' + target.name + '\n' +
          'Accuracy: ' + target.accuracy + '\n' +
          'Major: ' + target.major + '\n' +
          'Minor: ' + target.minor
        })}
        {detected && !detail && text.field({ text: 'Beacon detected' })}
        {!detected && text.field({ text: 'No beacon detected' })}
      </Div>
    )
  }
}
