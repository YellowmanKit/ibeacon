import React from 'react'
import Component from 'ibeacon/src/component/Component'
import { View as Div } from 'react-native'

export default class OptionField extends Component {

  content({ text, store: { ui: { style, color } } }, { title, value, onChange, options }){
    return(
      <Div style={{ ...this.size(0.4, 0.075), ...style.flexRowCC, justifyContent: 'space-evenly', backgroundColor: 'white' }}>
        {text.field({ text: title })}
        {options.map(option=> text.tiny(
          { text: option.text, onPress: ()=>{ onChange(option.value) }, key: option.value },
          { color: value === option.value? 'grey': color.grey[0] }))}
      </Div>
    )
  }

}
