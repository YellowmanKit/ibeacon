import React from 'react'
import Component from 'ibeacon/src/component/Component'
import { View as Div } from 'react-native'

export default class TextField extends Component {

  content({ text, input, store: { ui: { style, color } } }, { title, value, onChangeText }){
    return(
      <Div style={{ ...this.size(0.4, 0.075), ...style.flexRowCC, backgroundColor: 'white' }}>
        {text.field({ text: title })}
        {input.text({ value, onChangeText, multiline: false }, { backgroundColor: color.grey[0] })}
      </Div>
    )
  }

}
