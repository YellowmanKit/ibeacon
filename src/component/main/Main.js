import React from 'react'
import Component from 'ibeacon/src/component/Component'
import { View as Div, Dimensions } from 'react-native'

import Page from './page/Page'

export default class Main extends Component {

  init({ action }){
    action.main.set('status', 'home')
    action.ui.set('window', { width: Dimensions.get('window').width, height: Dimensions.get('window').height * 1.1 } )
  }

  content(app){
    return this.main(app)
  }

  main({ store: { main, ui: { window, style, color } } }){
    return(
      <Div style={{...window, ...style.flexColCC, backgroundColor: 'black' }}>
        <Page app={this.app}/>
      </Div>
    )
  }

}
