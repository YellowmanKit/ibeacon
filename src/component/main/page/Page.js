import React from 'react'
import Component from 'ibeacon/src/component/Component'

import Home from './home/Home'

export default class Page extends Component {

  content(app){
    switch (app.store.main.status) {
      case 'home':
        return <Home app={app}/>
      default:
        return null
    }
  }

}
