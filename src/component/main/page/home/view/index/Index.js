import React from 'react'
import { View as Div } from 'react-native'
import View from '../View'

import Install from './subView/installation/Install'
import Succeed from './subView/installation/Succeed'
import Failed from './subView/installation/Failed'

import Monitor from './subView/monitoring/Monitor'
import Begin from './subView/monitoring/Begin'
import Hacked from './subView/monitoring/Hacked'
import Status from './subView/monitoring/Status'

import Start from './subView/Start'
import Wait from './subView/Wait'

export default class Index extends View {

  init({ action, store }){
    action.page.set('nav', {
      title: ['INDEX']
    })
    action.page.setSubView('start', true)
  }

  view({ text, store: { ui: { window, style, color } } }){
    return(
      <Div style={{ ...window, ...style.flexColCS, backgroundColor: 'white' }}>
        {this.verGap(0.025)}
        <Div style={{ ...style.flexRowCS }}>
          {text.bigBold({ text: 'Travel' }, { color: 'blue' })}
          {text.bigBold({ text: 'Safer' }, { color: 'red' })}
        </Div>
        {text.small({ text: 'PlatingInstallation &' }, { color: 'green' })}
        {text.small({ text: 'Monitoring App' }, { color: 'green' })}
        {this.verGap(0.05)}
        {this.subView(this.app)}
      </Div>
    )
  }

  subView({ store: { page: { subView } } }){
    switch (subView) {
      case 'start':
        return <Start app={this.app}/>

      case 'install':
        return <Install app={this.app}/>
      case 'succeed':
        return <Succeed app={this.app}/>
      case 'failed':
        return <Failed app={this.app}/>

      case 'monitor':
        return <Monitor app={this.app}/>
      case 'hacked':
        return <Hacked app={this.app}/>
      case 'begin':
        return <Begin app={this.app}/>
      case 'status':
        return <Status app={this.app}/>

      case 'wait':
        return <Wait app={this.app}/>
      default:
        return null
    }
  }

}
