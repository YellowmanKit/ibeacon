import React from 'react'
import { View as Div } from 'react-native'
import SubView from '../../../SubView'

export default class Hacked extends SubView {

  subView({ text, button, input, action, store: { ui: { style, color }, read: { plate } } }){
    return(
      <Div style={this.subViewStyle}>
        {text.field({ text: 'Car/Venue No. ' + plate.number })}
        {this.verGap(0.05)}
        {text.failed({ text: 'Plate hacked' })}
        {text.failed({ text: 'Please contact management office' })}
        {this.verGap(0.35)}
        {button.rectRed({ text: 'RETURN', onPress: ()=>{ action.page.setSubView('monitor') } })}
      </Div>
    )
  }

}
