import React from 'react'
import { View as Div } from 'react-native'
import SubView from '../../../SubView'

import Detect from 'ibeacon/src/component/element/Detect'

import TextField from 'ibeacon/src/component/element/field/TextField'

export default class Install extends SubView {

  subView({ text, button, input, action, store: { ui: { style, color }, read: { monitor, plate, beacons } } }){
    const detected = beacons.length > 0
    return(
      <Div style={this.subViewStyle}>
        <TextField app={this.app} title={'Car/Venue No. : '} value={plate.number}
        onChangeText={newText=>{ action.read.set('plate', { ...plate, number: newText }) }}/>
        <TextField app={this.app} title={'Name : '} value={plate.name}
        onChangeText={newText=>{ action.read.set('plate', { ...plate, name: newText }) }}/>
        {this.verGap(0.02)}
        <Detect app={this.app}/>
        {this.verGap(0.02)}
        {detected && button.rectGreen({ text: 'SUBMIT', onPress: ()=>{ action.read.install({ beacon: beacons[0], plate }) } })}
        {this.verGap(0.02)}
        {button.rectRed({ text: 'RETURN', onPress: ()=>{ action.page.setSubView('start') } })}
      </Div>
    )
  }

}
