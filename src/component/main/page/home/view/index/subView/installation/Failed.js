import React from 'react'
import { View as Div } from 'react-native'
import SubView from '../../../SubView'

export default class Failed extends SubView {

  subView({ text, button, input, action, store: { ui: { style, color }, read: { plate } } }){
    return(
      <Div style={this.subViewStyle}>
        {text.info({ text: 'Plate Type : ' + plate.type + '\nCar/Venue No. ' + plate.number })}
        {this.verGap(0.05)}
        {text.failed({ text: 'Plate installation failed' })}
        {this.verGap(0.35)}
        {button.rectRed({ text: 'RETURN HOME', onPress: ()=>{ action.page.setSubView('start') } })}
      </Div>
    )
  }

}
