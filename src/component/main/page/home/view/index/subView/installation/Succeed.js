import React from 'react'
import { View as Div } from 'react-native'
import SubView from '../../../SubView'

export default class Succeed extends SubView {

  subView({ text, button, input, action, store: { ui: { style, color }, read: { plate, response } } }){
    return(
      <Div style={this.subViewStyle}>
        {text.info({ text: 'Plate Type : ' + plate.type + '\nCar/Venue No. ' + plate.number })}
        {this.verGap(0.05)}
        {text.field({ text: 'Plate installed successfully' })}
        {this.verGap(0.05)}
        {text.tiny({ text: 'Server response: \n' + JSON.stringify(response) })}
        {this.verGap(0.1)}
        {button.rectRed({ text: 'RETURN HOME', onPress: ()=>{ action.page.setSubView('start') } })}
      </Div>
    )
  }

}
