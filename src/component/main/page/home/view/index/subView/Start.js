import React from 'react'
import { View as Div, BackHandler } from 'react-native'
import DeviceInfo from 'react-native-device-info';
import SubView from '../../SubView'

import TextField from 'ibeacon/src/component/element/field/TextField'
import OptionField from 'ibeacon/src/component/element/field/OptionField'

export default class Start extends SubView {

  init({ action, store: { read: { phone } } }){
    const model = DeviceInfo.getModel()
    const osVersion = DeviceInfo.getSystemVersion()
    const manu = DeviceInfo.getManufacturer().then(result => {
      action.read.set('phone', { ...phone, model, osVersion, manu: result })
    })
  }

  subView({ text, button, input, action, store: { ui: { style, color }, read: { phone, beacon, setting } } }){
    return(
      <Div style={this.subViewStyle}>
        {text.info({ text:
          'Manufacturer: ' + phone.manu + '\n' +
          'Model: ' + phone.model + '\n' + (this.IOS? 'IOS':'Android') +
          ' Version: ' + phone.osVersion
        })}
        <TextField app={this.app} title={'Beacon Major : '} value={beacon.major}
        onChangeText={newText=>{ action.read.set('beacon', { ...beacon, major: newText }) }}/>
        {this.verSep(0.6)}
        <OptionField app={this.app} title={'Beacon Type : '} value={setting.type}
        options={[{ text: 'Old', value: 'old' }, { text: 'New', value: 'new' }]}
        onChange={newValue=>{ action.read.set('setting', { ...setting, type: newValue }) }}/>
        {this.verSep(0.6)}
        <OptionField app={this.app} title={'Mode : '} value={setting.mode}
        options={[{ text: 'Encrypted', value: 'encrypted' }, { text: 'Normal', value: 'normal' }]}
        onChange={newValue=>{ action.read.set('setting', { ...setting, mode: newValue }) }}/>
        {this.verSep(0.6)}
        {this.verGap(0.04)}
        {button.rectGreen({ text: 'PLATE INSTALLATION', onPress: ()=>{ action.page.setSubView('install') } })}
        {this.verGap(0.02)}
        {button.rectGreen({ text: 'PLATE MONITORING', onPress: ()=>{ action.page.setSubView('monitor') } })}
        {this.verGap(0.02)}
        {button.rectRed({ text: 'QUIT PROGRAM', onPress: ()=>{ BackHandler.exitApp() } })}
      </Div>
    )
  }

}
