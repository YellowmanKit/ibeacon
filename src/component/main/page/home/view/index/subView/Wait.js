import React from 'react'
import { View as Div } from 'react-native'
import SubView from '../../SubView'

export default class Wait extends SubView {

  subView({ text, button, input, action, store: { ui: { style, color }, read: { monitor, plate } } }){
    return(
      <Div style={this.subViewStyle}>
        {text.field({ text: 'Loading...' })}
      </Div>
    )
  }

}
