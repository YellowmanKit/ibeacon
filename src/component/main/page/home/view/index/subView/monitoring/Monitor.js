import React from 'react'
import { View as Div } from 'react-native'
import SubView from '../../../SubView'
import { nameParse } from 'ibeacon/src/utility/Func'
import TextField from 'ibeacon/src/component/element/field/TextField'
import OptionField from 'ibeacon/src/component/element/field/OptionField'
import Detect from 'ibeacon/src/component/element/Detect'

export default class Monitor extends SubView {

  subView({ text, button, input, action, store: { ui: { style, color }, read: { monitor, plate, setting, beacons } } }){
    return(
      <Div style={this.subViewStyle}>
        <Detect app={this.app}/>
        <TextField app={this.app} title={'Car/Venue No. : '} value={plate.number}
        onChangeText={newText=>{ action.read.set('plate', { ...plate, number: newText }) }}/>
        <TextField app={this.app} title={'Scan Time(sec) : '} value={monitor.scanTime}
        onChangeText={newText=>{ if(newText && isNaN(newText)){ return }
          action.read.set('monitor', { ...monitor, scanTime: newText }) }}/>
        <TextField app={this.app} title={'Report Time(min) : '} value={monitor.reportTime}
        onChangeText={newText=>{ if(newText && isNaN(newText)){ return }
          action.read.set('monitor', { ...monitor, reportTime: newText }) }}/>
        <TextField app={this.app} title={'Confirm Range(meter) : '} value={monitor.confirmRange}
        onChangeText={newText=>{ if(newText && isNaN(newText)){ return }
          action.read.set('monitor', { ...monitor, confirmRange: newText }) }}/>
        <TextField app={this.app} title={'Signal Range(meter) : '} value={monitor.signalRange}
        onChangeText={newText=>{ if(newText && isNaN(newText)){ return }
          action.read.set('monitor', { ...monitor, signalRange: newText }) }}/>
        <TextField app={this.app} title={'Minimum Signal Count : '} value={monitor.minSignalCount}
        onChangeText={newText=>{ if(newText && isNaN(newText)){ return }
          action.read.set('monitor', { ...monitor, minSignalCount: newText }) }}/>
        {this.verGap(0.02)}
        {beacons.length > 0 && button.rectGreen({ text: 'SUBMIT', onPress: ()=>{
          action.read.monitor({
            beacon: beacons[0],
            scanned_number: nameParse(setting.type, beacons[0].name, setting.mode),
            serial_code: beacons[0].uuid + beacons[0].major + beacons[0].minor,
            input_number: plate.number }) } })}
        {this.verGap(0.02)}
        {button.rectRed({ text: 'RETURN', onPress: ()=>{ action.page.setSubView('start') } })}
      </Div>
    )
  }

}
