import React from 'react'
import { View as Div } from 'react-native'
import SubView from '../../../SubView'

import Detect from 'ibeacon/src/component/element/Detect'

export default class Status extends SubView {

  subView({ text, button, input, action, store: { ui: { style, color }, read: { beacon, plate } } }){
    return(
      <Div style={this.subViewStyle}>
        {text.info({ text:
          'Plate No. : ' + plate.number + '\n' +
          'Name: ' + beacon.name + '(' + beacon.scanned_number + ')\n' +
          'UUID: ' + beacon.uuid + '\n' +
          'Major: ' + beacon.major + '\n' +
          'Minor: ' + beacon.minor
        })}
        {this.verGap(0.025)}
        <Detect app={this.app} detail={true} uuid={beacon.uuid}/>
        {this.verGap(0.025)}
        {button.rectRed({ text: 'RETURN', onPress: ()=>{ action.page.setSubView('begin') } })}
      </Div>
    )
  }

}
