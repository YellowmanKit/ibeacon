import React from 'react'
import { View as Div } from 'react-native'
import SubView from '../../../SubView'

export default class Begin extends SubView {

  subView({ text, button, input, action, store: { ui: { style, color }, read: { plate } } }){
    return(
      <Div style={this.subViewStyle}>
        {text.field({ text: 'Car/Venue No. ' + plate.number })}
        {text.field({ text: 'Number match' })}
        {text.field({ text: 'You may start monitoring now.' })}
        {this.verGap(0.35)}
        {button.rectGreen({ text: 'START MONITORING', onPress: ()=>{ action.page.setSubView('status') } })}
        {this.verGap(0.02)}
        {button.rectRed({ text: 'RETURN', onPress: ()=>{ action.page.setSubView('monitor') } })}
      </Div>
    )
  }

}
