import React from 'react'
import { View as Div, ScrollView } from 'react-native'
import Component from 'ibeacon/src/component/Component'

export default class View extends Component {

  content({ store: { ui: { style, color } } }){
    return(
      <ScrollView style={{...this.size(1,1), backgroundColor: color.yellow[0] }}>
        {this.view(this.app, this.state, this.props)}
      </ScrollView>
    )
  }

}
