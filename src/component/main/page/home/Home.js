import React from 'react'
import { View as Div } from 'react-native'
import Component from 'ibeacon/src/component/Component'

import Index from './view/index/Index'

export default class Home extends Component {

  content(app){
    return this.home(app)
  }

  home({ store: { page: { view }, ui: { window, style, color } } }){
    return(
      <Div style={{ ...window, ...style.flexColSC, backgroundColor: color.grey[3] }}>
        {this.view(view)}
      </Div>
    )
  }

  view(view){
    switch (view) {
      case 'index':
        return <Index app={this.app}/>
      default:
        return null;
    }
  }

}
