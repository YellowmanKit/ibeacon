import React from 'react'
import Component from 'ibeacon/src/component/Component'
import { View as Div, Text, TouchableOpacity, Image, TextInput } from 'react-native'

export default class Native extends Component {

  textInput({ value, placeholder, onChangeText, isPassword, multiline, keyboardType }, style){
    return(
      <TextInput
        value={value}
        placeholder={placeholder}
        onChangeText={onChangeText}
        style={style}
        secureTextEntry={isPassword}
        multiline={multiline}
        textAlignVertical={'top'}
        allowFontScaling={true}
        keyboardType={keyboardType}
      />
    )
  }

  touchableOpacity({ background, icon, text, onPress, key, disabled }, style){
    return(
      <TouchableOpacity style={{ opacity: disabled? 0.1:1, ...this.ui.style.flexColCC, ...style.view }} onPress={disabled? null: onPress} key={key}>
        {!!background && this.background(background)}
        {text && this.text({ text }, style.text)}
        <Image style={style.icon} source={icon?icon:null} />
      </TouchableOpacity>
    )
  }

  text({ text, onPress, key }, style){
    return <Text style={style} onPress={onPress} key={key}>{text}</Text>
  }

}
