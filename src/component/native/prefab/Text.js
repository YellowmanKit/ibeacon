import React from 'react'
import Native from '../Native'

export default class Text extends Native {

  failed(option, style){
    return this.text(option, {...this.fontSize(0.025), fontWeight: 'normal',
    color: 'red', ...style })
  }

  field(option, style){
    return this.text(option, {...this.fontSize(0.025), fontWeight: 'normal',
    color: 'grey', ...style })
  }

  info(option, style){
    return this.text(option, {...this.fontSize(0.03), fontWeight: 'normal', backgroundColor: 'yellow',
    color: 'grey', padding: 10, ...style })
  }

  bigBold(option, style){
    return this.text(option, {...this.fontSize(0.075), fontWeight: 'bold', ...style })
  }

  small(option, style){
    return this.text(option, {...this.fontSize(0.03), fontWeight: 'normal', ...style })
  }

  tiny(option, style){
    return this.text(option, {...this.fontSize(0.02), fontWeight: 'normal', ...style })
  }

}
