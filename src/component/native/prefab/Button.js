import React from 'react'
import Native from '../Native'

export default class Button extends Native{

  rectGreen(option){
    return this.rect({ ...option }, { view: { backgroundColor: '#35ad31' }})
  }

  rectRed(option){
    return this.rect({ ...option }, { view: { backgroundColor: 'red' }})
  }

  rect(option, style){
    return this.touchableOpacity(option, {
      view: { ...style.view, ...this.ui.style.flexColCC, ...this.size(0.3, 0.06)},
      text: {...this.fontSize(0.0175), ...{ color: 'black', fontWeight: 'bold', textAlign: 'center' } } })
  }

}
