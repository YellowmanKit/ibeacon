import React from 'react'
import Native from '../Native'

export default class Input extends Native{

  text(option, style){
    return this.textInput({ ...option, placeholder: '', secureTextEntry: false }, { ...this.size(0.15, 0.05), ...style })
  }

}
