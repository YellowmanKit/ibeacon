import axios from 'axios'
import { to, success, action, API } from '../../redux'

export const status = ({ scanned_number, username, monitorStatus }) => {
  return async dispatch => {
    dispatch(action.set('subView', 'wait'))
    var err, res
    const query = '?carNumber=' + scanned_number + '&user=' + serial_code + '&monitorStatus=' + monitorStatus;
    [err, res] = await to(axios.get(API + 'beacon_carMonitor.php' + query))
    if(err){ return dispatch(action.set('status', 'failed')) }
    dispatch(action.set('response', res.data))
    if(res.data.length === 0){
      dispatch(action.set('status', monitorStatus))
    }else{
      dispatch(action.set('status', 'failed'))
    }
  }
}

export const monitor = ({ beacon, scanned_number, serial_code, input_number }) => {
  return async dispatch => {
    dispatch(action.set('subView', 'wait'))
    var err, res
    const query = '?carNumber=' + scanned_number + '&uuid=' + serial_code + '&input=' + input_number;
    [err, res] = await to(axios.get(API + 'beacon_carInfoCheck.php' + query))
    if(err){ return dispatch(action.set('subView', 'hacked')) }
    dispatch(action.set('response', res.data))
    if(res.data.length === 0){
      dispatch(action.set('beacon', { ...beacon, scanned_number }))
      dispatch(action.set('subView', 'status'))
    }else{
      dispatch(action.set('subView', 'hacked'))
    }
  }
}

export const install = ({ beacon, plate }) => {
  return async dispatch => {
    dispatch(action.set('subView', 'wait'))
    var err, res
    const query = '?carNumber=' + plate.number + '&uuid=' + beacon.uuid + '&user=' + plate.name;
    [err, res] = await to(axios.get(API + 'beacon_carInstall.php' + query))
    if(err){ return dispatch(action.set('subView', 'failed')) }
    dispatch(action.set('response', res.data))
    console.log(res.data)
    if(res.data.includes('InstallStatus=\"Success\"')){
      dispatch(action.set('beacon', {
        installed: true,
        uuid: beacon.uuid,
        major: beacon.major.toString(),
        minor: beacon.minor.toString()
      }))
      dispatch(action.set('subView', 'succeed'))
    }else{
      dispatch(action.set('subView', 'failed'))
    }
  }
}
