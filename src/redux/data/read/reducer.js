import { reducer } from '../../redux'

export const read = (
state = {
  phone: {
    manu: '',
    model: '',
    osVersion: ''
  },
  setting: {
    type: 'old',
    mode: 'encrypted'
  },
  monitor: {
    scanTime: '15',
    reportTime: '1',
    confirmRange: '3',
    signalRange: '6',
    minSignalCount: '4'
  },
  plate: {
    name: 'Anthony Yeh',
    number: 'AB805',
    type: 'None'
  },
  beacon: {
    name: '',
    uuid: '',
    major: '',
    minor: ''
  },
  response: {},
  region: {},
  beacons: [],
  status: ''
}, action) => reducer(state, action, result)

const result = (state, { type, payload }) => {
  switch (type) {
    default:
      return state
  }
}
