import { bindActionCreators } from 'redux'
import * as main from './control/main/action'
import * as ui from './control/ui/action'
import * as page from './control/page/action'

import * as read from './data/read/action'

const add = (action, dispatch) => {
  action.set = (key, value) => { return { type: 'set', payload: { key, value } } }
  return bindActionCreators(action, dispatch)
}

export default actions = dispatch => { return { action: {
  main: add(main, dispatch),
  ui: add(ui, dispatch),
  page: add(page, dispatch),
  read: add(read, dispatch)
}}}
