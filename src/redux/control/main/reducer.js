import { reducer } from '../../redux'

export const main = (
state = {
  status: 'init',
  version: 'v1.0.0'
}, action) => reducer(state, action, result)

const result = (state, { type, payload }) => {
  switch (type) {
    default:
      return state
  }
}
