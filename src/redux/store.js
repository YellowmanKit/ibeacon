import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import { main } from './control/main/reducer'
import { ui } from './control/ui/reducer'
import { page } from './control/page/reducer'

import { read } from './data/read/reducer'

const reducer = combineReducers({
  main, ui, page,
  read
})

export default createStore(reducer, {}, applyMiddleware(thunk))
