import { decrypt, encrypt } from './Encrypt'
export const nameParse = (beacon_type, beacon_name, mode) => {
  var result = ''
  switch (beacon_type){
    case 'old':
      if(mode === 'encrypted') {
        sub_name = beacon_name.substring(0, beacon_name.indexOf('#'))
        beacon_name = decrypt(sub_name)
        result = beacon_name.substring(4).replace('\"','X')
      }else {
        result = beacon_name.substring(4, beacon_name.indexOf('#'))
      }
      break
    case 'new':
      result = beacon_name.substring(1,beacon_name.indexOf('#'))
      break
  }
  return result
}
