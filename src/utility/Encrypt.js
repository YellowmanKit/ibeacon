export const encrypt = str => encoder_Lookup(encoder_simpleCaesar(encoder_runlength(str),3))
export const decrypt = str => decoder_runlength(decoder_simpleCaesar(decoder_Lookup(str),3))

const originCode = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8','9','0','-','=','!','@','$','%','^','&','*','(',')','_','+','[',']','\\',';','\'','.','/','{','}','|',':','\'','<','>','?']
const encryptedCode = ['[','B','7',']','*','Q','i','=','V','3','0','\'','W','u','I','2','!','E','v','(','y','$','K','+','p','D','5','f','k','@','}',')','&','-','<','6','Y','%','^','8','A','G','m','g','r','N','\'','S','Z','?','w','h','H','b','O','R','P','c','x','J','l','M','n','t','.',';','L',':','X','j','o','{','>','|','z','F','\\','T','q','9','s','4','_','1','U','a','e','d','/','C']

const decoder_runlength = str =>{
  const substituteNumber = ['<','}','+','&','%','$','|','?','!','[']
  var decryptedString = ''
  var count=0
  for(var i=0;i<str.length;i++){
    const subStr = str.substring(i, i+1)
    const index = substituteNumber.indexOf(subStr)
    if(index==-1){
      if(count>0){
        for (var j=0;j<count;j++){
          decryptedString += subStr
        }
        count = 0
      }else{
        decryptedString += subStr
      }
    }else{
      count = 10 * count + index
    }
  }
  return decryptedString
}

const decoder_simpleCaesar = (str, offset) =>{
  const de_offset = offset % originCode.length
  var decryptedString = ''
  for(var i=0;i<str.length;i++){
    const subStr = str.substring(i, i + 1)
    if(originCode.indexOf(subStr) === -1){
      decryptedString += subStr
    }else{
      decryptedString += originCode[(originCode.indexOf(subStr) + de_offset) % originCode.length]
    }
  }
  return decryptedString
}

const decoder_Lookup = str =>{
  var decryptedString = ''
  for(var i=0;i<str.length;i++){
    const subStr = str.substring(i, i+1)
    if(encryptedCode.indexOf(subStr)==-1){
      decryptedString+=subStr
    }else {
      decryptedString += originCode[encryptedCode.indexOf(subStr)]
    }
  }
  return decryptedString
}

const encoder_runlength = (str) =>{
  const originNumber = ['0','1','2','3','4','5','6','7','8','9']
  const substituteNumber = ['<','}','+','&','%','$','|','?','!','[']
  var encryptedString = ''
  var currentStr = str.substring(0, 1)
  var count = 1
  if (str.length === 1){
    encryptedString = str
  }
  for(var i=1;i<str.length;i++){
    const subStr = str.substring(i, i+1)
    if(subStr === currentStr){
      count++
    }else{
      if(count>1){
        for(var j=0;j<count.toString().length;j++){
          const k = count.toString().substring(j,j+1)
          encryptedString += substituteNumber[originNumber.indexOf(k)]
        }
        encryptedString += currentStr
        count=1
      }else{
        encryptedString += currentStr
      }
    }
    currentStr = subStr
    if(i === str.length-1){
      if(count>1){
        for(var j=0;j<count.toString().length;j++){
            const k = count.toString().substring(j,j+1)
            encryptedString += substituteNumber[originNumber.indexOf(k)]
        }
        encryptedString+=subStr
        count=1
      }else{
        encryptedString+=subStr
      }
    }
  }
  return encryptedString
}

const encoder_simpleCaesar = (str, offset) =>{
  var en_offset = offset%originCode.length
  var encryptedString = ''
  for(var i=0;i<str.length;i++){
    const subStr = str.substring(i, i+1)
    if(originCode.indexOf(subStr)==-1){
        encryptedString+=subStr
    }else{
      var index= (originCode.indexOf(subStr)-en_offset)
      index = index >= 0?index:(index+originCode.length)
      encryptedString += originCode[index]
    }
  }
  return encryptedString
}

const encoder_Lookup = str =>{
  var encryptedString = ''
  for(var i=0;i<str.length;i++){
    const subStr = str.substring(i, i+1)
    if(originCode.indexOf(subStr)==-1){
      encryptedString+=subStr
    }else {
      encryptedString += encryptedCode[originCode.indexOf(subStr)]
    }
  }
  return encryptedString
}
