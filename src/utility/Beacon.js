import React from 'react'
import { Platform, NativeEventEmitter, PermissionsAndroid, DeviceEventEmitter } from 'react-native'

import Kontakt, { KontaktModule } from 'react-native-kontaktio'
const { connect, init, startDiscovery, startScanning } = Kontakt
const kontaktEmitter = new NativeEventEmitter(KontaktModule)

export default class Beacon {

  async set(app){
    if (!this.IOS) {
      const granted = await this.permission()
      if (granted) {
        await connect()
        await startScanning()
      } else {
        Alert.alert(
          'Permission error',
          'Location permission not granted. Cannot scan for beacons',
          [{text: 'OK', onPress: () => console.log('OK Pressed')}],
          {cancelable: false},
        )
        return
      }
    } else {
      await init()
      await startDiscovery()
    }

    if (Platform.OS !== 'ios') {
      DeviceEventEmitter.addListener('beaconsDidUpdate', data => {
        this.detected(app, data)
      })
    } else {
      kontaktEmitter.addListener('didDiscoverDevices', data => {
        this.detected(app, data)
      })
    }
  }

  async permission(){
    //console.log('Ask Permission')
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        {
          title: 'Location Permission',
          message:
            'iBeacon app needs to access your location in order to use bluetooth beacons.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        //console.log('Permission Granted')
        return true;
      } else {
        //console.log('Permission Denied')
        return false;
      }
    } catch (err) {
      console.warn(err);
      return false;
    }
  }

  detected({ action }, { beacons, region }){
    var data = beacons.map(beacon => {
      return {
        name: beacon.name,
        accuracy: beacon.accuracy.toFixed(4),
        major: beacon.major,
        minor: beacon.minor,
        uuid: beacon.uuid
      } })
    data.sort((a,b)=> a.accuracy > b.accuracy)
    action.read.set('beacons', data)
    action.read.set('region', region)
    this.clear({ action })
  }

  clear({ action }){
    clearTimeout(this.killer)
    this.killer = setInterval(()=> {
      action.read.set('beacons', [])
    }, 3000)
  }

}
