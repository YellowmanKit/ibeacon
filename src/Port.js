import React from 'react'
import Main from 'ibeacon/src/component/main/Main'

import Text from './component/native/prefab/Text'
import Button from './component/native/prefab/Button'
import Input from './component/native/prefab/Input'

class Port extends React.Component {

  constructor(props){
    super(props)
    this.state = {
    }
  }

  render(){
    return this.main(this.props)
  }

  main({ store, action }){
    const props = {
      app: { store, action }
    }
    return(
      <Main app={{
        ...this.state, ...props.app,
        text: new Text(props),
        button: new Button(props),
        input: new Input(props)
      }}/>
    )
  }

}

import actions from './redux/actions'
import { connect } from 'react-redux'
export default connect(store => { return { store } }, dispatch => { return actions(dispatch) })(Port)
