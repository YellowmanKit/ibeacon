import React from 'react'
import { Provider } from 'react-redux'
import store from './src/redux/store'
import Port from './src/Port'

const App: () => React$Node = () => {
  return <Provider store={store}><Port/></Provider>
}

export default App
